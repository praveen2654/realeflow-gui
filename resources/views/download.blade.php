@extends('app')
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
@section('content')
<form method="POST" action="{{route('uploadfile')}}" aria-label="{{__('Upload')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
<ul class="form-style-1">
    <li>
        <button type="submit" class="btn btn-primary">
            {{ __('Download') }}
        </button>
    </li>
</ul>
</form>
@endsection


