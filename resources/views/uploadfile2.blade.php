@extends('app')
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
@section('content')
<form method="POST" action="{{route('uploadfile')}}" aria-label="{{__('Upload')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
<ul class="form-style-1">
    <li><label>Start </label><input type="text" name="start_index" class="field-divided" placeholder="Start Index" /> <input type="text" name="end_index" class="field-divided" placeholder="End Index" /></li>
    <li>
    <input class="field-long" type="file" name="fileToUpload" id="fileToUpload">
    </li>
    <li>
        <button type="submit" class="btn btn-primary">
            {{ __('Upload') }}
        </button>
    </li>
</ul>
</form>
@endsection


