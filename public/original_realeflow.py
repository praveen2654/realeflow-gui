from __future__ import division
import urllib
import json
import pandas as pd
import numpy as np
import datetime
import requests
import sys

filename = sys.argv[1]
out_filename = filename.replace(".xlsx", "_out.csv")
start = 0

if len(sys.argv) > 2:
    start = int(sys.argv[2])
else:
    start = 1

df = pd.read_excel(filename, dtype=str)
df = df.replace('$', '')
df = df.astype(str)
cols = df.columns.tolist()
origColLen = len(cols)


def get_data(address):
    link = "https://addy.flipcomp.com/2018091901/search/" + address + \
           "?key=fb67bd77c4a9eeef1dbcdb0c746b3f74de9f6316&entities=address%2Ccity%2Ccounty%2Czip"
    response = session.get(link).json()
    id = response[0]['address']['hash']
    property_link = "https://awesome.realeflow.com/publy/property/" + str(id)
    print(property_link)
    data = session.get(property_link).json()
    return data


if __name__ == '__main__':
    payload = {'Email': 'bob.newbern99@gmail.com',
               'Password': 'realox1'}
    POST_LOGIN_URL = 'https://awesome.realeflow.com/Account/Account/LogOn?ReturnUrl=%2FMarketing%2FLeads%2FIndex%2FProperty'
    with requests.Session() as session:
        post = session.post(POST_LOGIN_URL, data=payload)
        if len(sys.argv) > 4:
            end = int(sys.argv[4])
        else:
            end = len(df.index)
        for i in range(start, end):
            try:
                data = {}
                address = df.iat[i, 0] + " " + df.iat[i, 1] + \
                    " " + df.iat[i, 2] + " " + \
                    str(df.iat[i, 3]).split("-", 1)[0]
                address = address.replace(" ", "%20")
                results = get_data(address)
            except:
                continue
            first_name = results['firstName']
            last_name = results['lastName']
            full_name = first_name+" "+last_name
            data["First Name"] = first_name
            data["Last Name"] = last_name
            data["Full Name"] = full_name
            data["Mailing Address"] = results['mailingAddress']
            data["Mailing City"] = results['mailingCity']
            data["Mailing State"] = results['mailingState']
            data["Mailing Zip"] = results['mailingZipCode']
            data["Home Type"] = results['homeType']
            data["House Style"] = results['houseStyle']
            data["Bed Count"] = results['bedrooms']
            data["Bath Count"] = results['bathrooms']
            data["Sqft"] = results['squareFeet']
            stories = results['assessor']['stories_nbr_code']
            data["Stories"] = int(stories)/100
            data["Year Built"] = results['yearBuilt']
            data["Lot Sqft"] = results['lotSquareFeet']
            data["AVM"] = str(results['assessor']['avm'])
            data["LTV"] = str(results['ltv'])+"%"
            data["Market Value"] = str(
                results['assessor']['market_total_value'])
            data["Assessed Value"] = str(
                results['assessor']['assd_total_value'])
            data["Assessed Year"] = results['assessor']['assd_year']
            annual_tax_payment = results['assessor']['tax_amount']
            data["Annual Tax Payment"] = str(annual_tax_payment/100)
            data["Tax Year"] = results['assessor']['tax_year']
            data["Subdivison Name"] = results['assessor']['subdivision_name']
            data["Legal Description"] = results['assessor']['legal_description']

            data["Condition"] = results['condition']
            data["Exterior"] = results['exterior']
            data["Interior Walls"] = results['interiorWalls']
            data["Basement"] = results['basement']
            data["Roof"] = results['roof']
            data["Roof Type"] = results['roofType']
            data["Water"] = results['water']
            data["Sewer"] = results['sewer']
            data["Loc Influence"] = results['locationInfluence']
            data["Heating"] = results['heating']
            data["Heating Type"] = results['heatingFuel']
            data["Air Conditioning"] = results['airConditioning']
            fireplace = results['fireplace']
            if int(fireplace) == 0:
                fireplace = 'No'
            elif int(fireplace) == 1:
                fireplace = 'Yes'
            data["Fireplace"] = fireplace
            data["Garage"] = results['garage']
            data["Patio"] = results['patio']
            data["Pool"] = results['pool']
            data["Porch"] = results['porch']

            if len(results['historyData']) > 0:
                date = results['historyData'][0]['recording_date']
                data["Recording Date"] = date[:-4] + \
                    "/"+date[-4:-2]+"/"+date[-2:]
                event = results['historyData'][0]['lookupCodes']['documentType']
                data["Sale Type"] = event
                sale_amount = results['historyData'][0]['sale_amount']
                if not sale_amount:
                    sale_amount = int(0)
                else:
                    sale_amount = str(sale_amount).replace('$', '')
                data["Sale Amount"] = sale_amount
                loan = results['historyData'][0]['first_mortgage_mortgage_amount']
                if not loan:
                    loan = int(0)
                else:
                    loan = str(loan).replace('$', '')
                data["Loan"] = loan
                lender = results['historyData'][0]['first_mortgage_lender_name']
                data["Lender"] = lender
                buyer = results['historyData'][0]['buyer_name_borrower_name']
                data["Buyer"] = buyer
                seller = results['historyData'][0]['seller_name']
                data["Seller"] = seller
            else:
                data["Recording Date"] = ""
                data["Sale Type"] = ""
                data["Sale Amount"] = ""
                data["Loan"] = ""
                data["Lender"] = ""
                data["Buyer"] = ""
                data["Seller"] = ""

            if i < 2:
                # Add column headers
                for k in data:
                    cols.append(k)

            # Write row data, cell by cell
            for j in range(origColLen, len(cols)):
                print(j)
                try:
                    df.at[i, list(data)[j]] = list(data.values())[j]
                except:
                    continue

df = df.replace('nan', '').replace('N/A', '')
df.columns = df.columns.str.split('.').str[0]
df.to_csv(out_filename)
