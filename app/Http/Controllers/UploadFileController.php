<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;
use App\ItemDetail;

class UploadFileController extends Controller
{

    public function index() {
      return view('uploadfile2');
  	}

   	public function showUploadFile(Request $request) {

  //  		$command = escapeshellcmd('rm -r ../storage/app/photos/*');
		// $output = shell_exec($command);

		$filename = $request->fileToUpload->getClientOriginalName();
      	$file = $request->fileToUpload->storeAs('photos', $filename);
      	
      	$start = $request->start_index;
      	$end = $request->end_index;
      	if (!$start) {
      		$start = "";
      	}
  		if (!$end) {
      		$end = "";
  		}
      	$command = escapeshellcmd('python3 original_realeflow.py ../storage/app/photos/'.$filename.' '.$start.' '.$end);
		$output = shell_exec($command);
		$storage_path = storage_path();
		return response()->download($storage_path.'/app/photos/data_out.csv');		

      // //Display File Name
      // echo 'File Name: '.$file->getClientOriginalName();
      // echo '<br>';
   
      // //Display File Extension
      // echo 'File Extension: '.$file->getClientOriginalExtension();
      // echo '<br>';
   
      // //Display File Real Path
      // echo 'File Real Path: '.$file->getRealPath();
      // echo '<br>';
   
      // //Display File Size
      // echo 'File Size: '.$file->getSize();
      // echo '<br>';
   
      // //Display File Mime Type
      // echo 'File Mime Type: '.$file->getMimeType();
   
      //Move Uploaded File
      // $destinationPath = 'uploads';
      // $file->move($destinationPath,$file->getClientOriginalName());
   	}
}
